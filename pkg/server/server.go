package server

import(
	"github.com/gofiber/fiber/v2"
	"fmt"
)

type ServHandler interface{
	RegisterRoutes(app *fiber.App)
}

type Server struct{
	App *fiber.App
}

func NewServer(handlers []ServHandler) *Server{
	app := fiber.New()
	server := Server{
		App: app,
	}
	for _, handler := range handlers {
		handler.RegisterRoutes(server.App)
	}

	return &server
}

func (s *Server) RunApp(port int) error{
	return s.App.Listen(fmt.Sprintf(":%d", port))
}