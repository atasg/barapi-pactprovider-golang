//go:build unit_test
// +build unit_test

package server_test

import (
	"testing"
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"barApi/pkg/server"
	"net/http"
	"github.com/phayes/freeport"
	"fmt"
)

type DummyHandler struct{

}
func (h *DummyHandler) RegisterRoutes(app *fiber.App){
	app.Get("/healthcheck", h.HealthCheck)
}

func (h *DummyHandler) HealthCheck(c *fiber.Ctx) error {
	return c.SendStatus(fiber.StatusOK)
}
func NewDummyHandler() *DummyHandler{
	return &DummyHandler{
	}
}
func TestWhenCallRunAppItShouldRunOnSpecificPort(t *testing.T){
	handler := NewDummyHandler()
	server := server.NewServer([]server.ServHandler{handler})

	port, err := freeport.GetFreePort()
	assert.Nil(t, err)
	go server.RunApp(port)
	c := http.Client{}
	url := fmt.Sprintf("http://127.0.0.1:%d/healthcheck", port)
	r, _ :=c.Get(url)

	assert.Equal(t, http.StatusOK, r.StatusCode)
}