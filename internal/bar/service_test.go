//go:build unit_test
// +build unit_test

package bar_test

import (
	"testing"
	"barApi/internal/bar"
	"github.com/stretchr/testify/assert"
)
func TestCreatePongs(t *testing.T){
	areaTest:= []struct{
		definition string
		request bar.PongRequest
		response bar.PongResponse
	}{
		{definition: "5 times", request: bar.PongRequest{Times:5}, response : bar.PongResponse{Pongs:[]string{"pong","pong","pong","pong","pong"}}},
		{definition: "2 times", request: bar.PongRequest{Times:2}, response : bar.PongResponse{Pongs:[]string{"pong","pong"}}},
		{definition: "3 times", request: bar.PongRequest{Times:3}, response : bar.PongResponse{Pongs:[]string{"pong","pong","pong"}}},

	}

	for _, tt := range areaTest{
		t.Run(tt.definition, func(t *testing.T){
			service := new(bar.Service)
			got := service.CreatePongs(&tt.request)
			assert.Equal(t, got, &tt.response)

		})
	}
}