//go:build unit_test
// +build unit_test

package bar_test

import (
	"testing"
	"net/http"
	"io/ioutil"
	"log"
	"encoding/json"
	"bytes"
	"barApi/internal/bar"
	"net/http/httptest"
	"strings"
	mocks "barApi/internal/mocks/bar"
	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/gofiber/fiber/v2"
)

const (
	pathPing = "/ping"
	pathHealth = "/healthcheck"
	returnUnprocessibleEntityLessThanZero = `{"status":"Unprocessible Entity", "message":"Invalid Content - Times should be greater than 0"}`
	returnUnproessibleEntityNoTimesKey = `{"status":"Unprocessible Entity", "message":"Invalid Content - No key that is named times"}`
	returnBadRequest = `{"status":"Bad Request", "message":"Invalid JSON"}`
	returnHealtyConnection = `{"status":"OK", "message":"Healty connection"}`

)

func CreateMockApp(t *testing.T) (*fiber.App , *mocks.MockHandlerService){
	app := bar.CreateApp()
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockHandlerService := mocks.NewMockHandlerService(mockCtrl)
	handler := bar.NewHandler(mockHandlerService)
	handler.RegisterRoutes(app)
	return app, mockHandlerService
}

func TestHappyHealthCheck(t *testing.T){
	app, _ := CreateMockApp(t)

	request := httptest.NewRequest(http.MethodGet, pathHealth,nil)
	request.Header.Add("Content-Type", "application/json")

	response, _ := app.Test(request)
	body, err := ioutil.ReadAll(response.Body)
	if err != nil{
		log.Fatalln(err)
	}
	assert.Equal(t, http.StatusOK, response.StatusCode)

	assert.JSONEq(t, returnHealtyConnection,string(body))
} 

func TestWhenBodyIsInvalid(t *testing.T){
	app ,_ := CreateMockApp(t)

	// create invalid json -- create a reader 
	r := strings.NewReader("my request")
	request := httptest.NewRequest(http.MethodPost, pathPing,r)
	request.Header.Add("Content-Type", "application/json")

	response, err := app.Test(request)
	if err != nil{
		log.Fatalf("An error occured %v", err)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil{
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.JSONEq(t, returnBadRequest, string(body))
}

func TestWhenBodyIsSyntacticallyValidButContentIsInvalid(t *testing.T){
	app ,_ := CreateMockApp(t)

	givenRequestBody := bar.PongRequest{
		Times:-3,
	}
	postBody, _ := json.Marshal(givenRequestBody)

	request := httptest.NewRequest(http.MethodPost, pathPing, bytes.NewBuffer(postBody))
	request.Header.Add("Content-Type", "application/json")

	response, err := app.Test(request)
	if err != nil{
		log.Fatalf("An error occured %v", err)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil{
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
	assert.JSONEq(t, returnUnprocessibleEntityLessThanZero, string(body))
}

func TestWhenBodyIsSyntacticallyValidButNoKeyNamedTimes(t *testing.T){
	app ,_ := CreateMockApp(t)

	postBody, _ := json.Marshal(map[string]int{
		"time": 5,
	})
	request := httptest.NewRequest(http.MethodPost, pathPing, bytes.NewBuffer(postBody))
	request.Header.Add("Content-Type", "application/json")

	response, err := app.Test(request)
	if err != nil{
		log.Fatalf("An error occured %v", err)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil{
		log.Fatalln(err)
	}

	assert.Equal(t,http.StatusUnprocessableEntity, response.StatusCode)
	assert.JSONEq(t,returnUnproessibleEntityNoTimesKey, string(body))
}


func TestWhenBodyIsValid(t *testing.T){
	app ,mockHandlerService := CreateMockApp(t)

	expectedInput := bar.PongRequest{Times:3}
	expectedReturn := bar.PongResponse{
		Pongs: []string{"pong","pong","pong"},
	}

	mockHandlerService.EXPECT().CreatePongs(&expectedInput ).Return(&expectedReturn)

	requestBody, err := json.Marshal(expectedInput)
	if err != nil {
		t.Fatal("can not Marshall request body")
	}

	request := httptest.NewRequest(http.MethodPost, pathPing, bytes.NewBuffer(requestBody))
	request.Header.Add("Content-Type", "application/json")

	response, _ := app.Test(request)

	body, _ := ioutil.ReadAll(response.Body)

	assert.Equal(t,http.StatusOK, response.StatusCode)
	assert.Equal(t, string(body),`{"pongs":["pong","pong","pong"]}` )
}