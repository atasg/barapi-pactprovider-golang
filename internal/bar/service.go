package bar

import (

)

type Service struct{
}

func NewService() *Service{
	return &Service{
	}
}

func (s *Service) CreatePongs(request *PongRequest) *PongResponse{
	times := request.Times
	list := []string{}
	for i:= 0; i<times;i++{
		list = append(list, "pong")
	}
	response := &PongResponse{
		Pongs: list,
	}
	return response
}