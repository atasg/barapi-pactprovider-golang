package bar

import (
	"encoding/json"
	"log"
	"github.com/gofiber/fiber/v2"
)

type (
	HandlerService interface {
		CreatePongs(request *PongRequest) *PongResponse
	}
	Handler struct {
		service HandlerService
	}
)

func NewHandler(service HandlerService) *Handler {
	return &Handler{
		service: service,
	}
}

func CreateApp() *fiber.App {
	app := fiber.New()
	return app
}

func (h *Handler) RegisterRoutes(app *fiber.App) {
	app.Get("/healthcheck", h.HealthCheck)
	app.Post("/ping", h.CreatePongs)
}

func (h *Handler) HealthCheck(c *fiber.Ctx) error {
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":  "OK",
		"message": "Healty connection",
	})
}

func (h *Handler) CreatePongs(c *fiber.Ctx) error {
	var request PongRequest

	// for bad request
	if err := c.BodyParser(&request); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "Bad Request",
			"message": "Invalid JSON",
		})
	}

	// if request body does not contain times key it should return 422(unprocessible entity) as status code
	// first we check for bad request then 422, so there will be no error for unmarshalling
	body := c.Body()
	var requestMap map[string]json.RawMessage
	err := json.Unmarshal(body, &requestMap)
	if err != nil {
		log.Fatal(err)
	}

	if _, err := requestMap["times"]; !err {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(fiber.Map{
			"status":  "Unprocessible Entity",
			"message": "Invalid Content - No key that is named times",
		})
	}
	// unprocessible entity  if times less than or equal to 0
	if request.Times <= 0 {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(fiber.Map{
			"status":  "Unprocessible Entity",
			"message": "Invalid Content - Times should be greater than 0",
		})
	}

	pongs := h.service.CreatePongs(&request)

	return c.Status(fiber.StatusOK).JSON(pongs)
}
