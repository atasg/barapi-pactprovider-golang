docker_build:
	docker build . -t service

docker_run:
	docker run --publish 8080:8080 service

provider_test: 
	go clean -testcache && go test -tags=pact ./... -v

unit_test:
	go clean -testcache && go test -tags=unit_test ./... -v

all_test: unit_test provider_test