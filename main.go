package main

import (
	"barApi/pkg/server"
	"barApi/internal/bar"
	"log"
)

func main(){
	service :=bar.NewService()
	handler := bar.NewHandler(service)
	serv := server.NewServer([]server.ServHandler{handler})
	err := serv.RunApp(8080)
	if err!=nil{
		log.Fatal(err)
	}
}