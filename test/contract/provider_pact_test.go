//go:build pact
// +build pact

package contract_test

import (
	"github.com/pact-foundation/pact-go/utils"
	"github.com/stretchr/testify/suite"
	"testing"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"barApi/internal/bar"
	"barApi/pkg/server"
	"os"
	"log"
	"github.com/joho/godotenv"
	"fmt"
)

type PactSuite struct{
	suite.Suite
}

func TestProvider(t *testing.T){
	suite.Run(t, new(PactSuite))
}
var stateHandlers = types.StateHandlers{
	"It will return pong list": func() error {
		return nil
	},
	"When test body is syntactically valid but content is invalid": func() error {
		return nil
	},
	"When there is no key named times in body": func() error {
		return nil
	},
}

func (s *PactSuite) TestProviderBarApi(){
	errLoad := godotenv.Load("../../.dev/.env")
	if errLoad != nil {
		log.Fatalln(errLoad.Error())
	}
	port,_ :=utils.GetFreePort()
	brokerBaseURL := os.Getenv("PACT_BROKER_BASE_URL")
	brokerToken := os.Getenv("PACT_BROKER_TOKEN")
	providerVersion := os.Getenv("PACT_PROVIDER_VERSION")
	providerBaseURL := fmt.Sprintf("http://localhost:%d",port)
	service :=bar.NewService()
	handler := bar.NewHandler(service)
	server := server.NewServer([]server.ServHandler{handler})
	
	go server.RunApp(port)

	pact:= dsl.Pact{
		Provider: "BarApi",
		DisableToolValidityCheck: true,
		Consumer: "FooClient",
		Host: "localhost",
	}

	_, err := pact.VerifyProvider(
		s.T(),
		types.VerifyRequest{
			ProviderBaseURL: providerBaseURL, 
			BrokerToken: brokerToken, 
			BrokerURL:brokerBaseURL,
			PublishVerificationResults: true,
			ProviderVersion: providerVersion,
			StateHandlers: stateHandlers,
			Tags:[]string{"master"},
		},
	)
	s.NoError(err)
}